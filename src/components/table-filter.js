import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, Button, TextField } from "@mui/material";
import { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Label, Row } from 'reactstrap';
import DATA_JSON from "./data-list";
function Filter() {
    // console.log(DATA_JSON);
    const [inputValue, setInputValue] = useState("");
    const [array, setArray] = useState(DATA_JSON);
    const [filterdata, setFilterData] = useState([]);
    
    const inputChange = (event) => {
        setInputValue(event.target.value);
    }
    const onBtnClick = () => {
        // console.log("Lọc thông tin");
        // console.log(inputValue);
        DATA_JSON.forEach(element => {
            if(element === inputValue) {
                setFilterData(filterdata.push(element));
            }
        });
        setArray(filterdata);
        console.log(filterdata);
        console.log(array);
    }
    return (
        <Grid>
            <Grid sx={{ p: 2 }}>
                <Row className="bg-light">
                    <Col xs={2} className="p-3">
                        <Label>Nhập nội dung lọc:</Label>
                    </Col>
                    <Col xs={8} className="pt-1">
                        <TextField id="outlined-basic" label="Input Data" variant="outlined" fullWidth onChange={inputChange} />
                    </Col>
                    <Col xs={2} className="p-3">
                        <Button variant="contained" color="secondary" onClick={onBtnClick}>Lọc</Button>
                    </Col>
                </Row>
            </Grid>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" sx={{ fontWeight: 'bold' }}>STT</TableCell>
                            <TableCell align="center" sx={{ fontWeight: 'bold' }}>Nội dung</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {array.map((row, index) => (
                            <TableRow
                                key={index}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row" align="center">
                                    {index + 1}
                                </TableCell>
                                <TableCell align="left">
                                    {row}
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    )
}
export default Filter;