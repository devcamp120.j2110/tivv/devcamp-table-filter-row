
import { Container } from "@mui/material";
import Filter from "./components/table-filter";


function App() {
  return (
    <Container>
      <Filter/>
    </Container>
  );
}

export default App;
